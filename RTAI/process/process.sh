#!/bin/bash
# Script para monitoramento do processo eucalyptus no Node Controller

# Escreve o cabeçalho de identificação dos dados
echo "%cpu virt.mem res.mem data hora" >> monitoramento-processo-nc.txt

echo "%cpu virt.mem res.mem data hora"

while [ True ]
do
   # Obtem o PID do processo eucalyptus
   pid=`ps aux | grep eucalyptus | grep "106 " | awk '{print $2}'`

   # Obtem os campos de interesse ( CPU(%),VSZ(KB), e RSS(KB) )
   cpu=`pidstat -u -h -p $pid -T ALL -r 60 1 | sed -n '4p' | awk '{print $6}'`
   mem=`cat /proc/$pid/stat | awk '{print $23/1024,$24*4}'`

   # Obtem o tempo atual, no formato RFC3339: AAAA-MM-DD HH:MM:SS 
   tempo=`date --rfc-3339=seconds`

   # Separa data e hora do tempo obtido
   data=`echo $tempo | cut -d\  -f1`
   hora=`echo $tempo | cut -d\  -f2| awk 'BEGIN{FS="-"}{print $1}'`
   
   # Mostre na tela as informações capturadas pelo script
   echo $cpu $mem $data $hora

   # Escreve no arquivo as informações capturadas
   echo $cpu $mem $data $hora>> monitoramento-processo-nc.txt

   # Executa o script a cada X unidade de tempo 
   #sleep 60

done
