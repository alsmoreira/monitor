#!/bin/bash
# Script para monitoramento de processos zumbis

# Escreve o cabeçalho de identificação dos dados
echo "num_zumbis data hora" >> monitoramento-zumbis.txt

echo "num_zumbis data hora"

while [ True ]
do
   # Obtem o tempo atual, no formato RFC3339: AAAA-MM-DD HH:MM:SS 
   tempo=`date --rfc-3339=seconds`
   
   # Armazena somente os campos de interesse
   num=`ps aux | awk '{if ($8~"Z"){print $0}}' | wc -l`
   listaZumbis=`ps aux | awk '{if ($8~"Z"){print $11}}'`

   # Separa data e hora do tempo obtido
   data=`echo $tempo | cut -d\  -f1`
   hora=`echo $tempo | cut -d\  -f2| awk 'BEGIN{FS="-"}{print $1}'`
   
   # Mostre na tela as informações capturadas pelos script
   echo $num $data $hora

   # Escreve no arquivo as informações do disco
   echo $num $data $hora>> monitoramento-zumbis.txt
   echo $data $hora>> monitoramento-lista-zumbis.txt
   echo $listaZumbis>> monitoramento-lista-zumbis.txt

   # Executa o script a cada X unidade de tempo 
   sleep 60

done

